using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreSystem : MonoBehaviour
{
    
    #region Singleton

    public static ScoreSystem instance;
    
    void Awake () 
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance of Inventory found!");
            return;
        }
        
        instance = this;
        
    }

    #endregion

    private float currentScore = 0;
    private float timeBonus = 0;
    
    //AddedScore = garbage score * distance from where the object was thrown to the garbage
    public float AddScore(float scoreFromGarbage, float distanceMul)
    {
        return currentScore += scoreFromGarbage * distanceMul;
    }
    
    //FinalScore = Score + (max time bonus * (TimeLeft/TimeLimit))
    public (float, float) CalculateFinalScore(float possibleTimeBonus, float timeLeft ,float timeLimit)
    {
        timeBonus = possibleTimeBonus * (timeLeft / timeLimit);
        currentScore += timeBonus;
        
        return (timeBonus, currentScore);
    }

}
