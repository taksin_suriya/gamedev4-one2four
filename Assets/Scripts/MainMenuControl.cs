using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuControl : MonoBehaviour
{
    public void StartButton(Button button)
    {
        SceneManager.LoadScene("GameplayScene");
}
    
    public void TutorialButton(Button button)
    {
        SceneManager.LoadScene("TutorialScene");
    }
    
    public void MainmenuButton(Button button)
    {
        SceneManager.LoadScene("MainMenuScene");
    }
    
    public void OptionButton(Button button)
    {
        if (!GameApplicationManager.Instance.IsOptionMenuActive)
        {
            SceneManager.LoadScene("OptionScene", LoadSceneMode.Additive);
            GameApplicationManager.Instance.IsOptionMenuActive = true;
        }
        
    }

    public void ExitButton(Button button)
    {
        Application.Quit();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
