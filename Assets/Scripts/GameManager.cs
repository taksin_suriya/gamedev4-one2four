using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    //Singleton
    #region Singleton

    public static GameManager instance;
    
    void Awake () 
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance of Inventory found!");
            return;
        }
        
        instance = this;
        
    }

    #endregion
    
    private float score;
    private float finalScore;
    private ScoreSystem _scoreSystem;

    #region Variables

    
    private CrabControl playerRef;
    
    private String sceneName;
    
    [SerializeField] private TextMeshProUGUI scoreNumText;
    
    private float fGarbageLeft;
    [SerializeField] private TextMeshProUGUI garbageNumText;

    [Header("Time Limit")]
    [Range(0, 99)]
    [SerializeField] private float minutes;
    [Range(0, 59)]
    [SerializeField] private float seconds;
    private float timeLimit;
    private bool timerIsActive;
    private float fTimeCounter;
    [SerializeField] private TextMeshProUGUI currentTimeText;
    
    private float timeBonus;
    [SerializeField] private float possibleTimeBonus;

    private bool resultScreenCalled = false;

    public GameObject pauseUI;
    
    
    #endregion
    
    // Start is called before the first frame update
    void Start()
    {
        sceneName = SceneManager.GetActiveScene().name;
        Debug.Log(sceneName);

        playerRef = GameObject.FindGameObjectWithTag("Player").GetComponent<CrabControl>();
        
        _scoreSystem = ScoreSystem.instance;

        //Find how many Garbage are left in the level
        fGarbageLeft = GameObject.FindGameObjectsWithTag("Garbage").Length;
        
        timeLimit = minutes * 60 + seconds;
        fTimeCounter = timeLimit;

        timerIsActive = true;
        resultScreenCalled = false;
    }
    
    // Update is called once per frame
    void Update()
    {
        //หยุดเวลา
        if (Input.GetKeyDown(KeyCode.Escape) && !resultScreenCalled)
        {
            if (Time.timeScale == 1)
            {
                Time.timeScale = 0;
                pauseUI.SetActive(true);
                Cursor.lockState = CursorLockMode.None;
            }
            else
            {
                Time.timeScale = 1;
                pauseUI.SetActive(false);
                Cursor.lockState = CursorLockMode.Locked;
            }
        }
        
        garbageNumText.text = fGarbageLeft.ToString();

        if (timerIsActive)
        {
            fTimeCounter -= Time.deltaTime;
        }

        TimeSpan time = TimeSpan.FromSeconds(fTimeCounter);
        currentTimeText.text = time.ToString(@"mm\:ss");
        
        //GameEnd Condition
        if ((fTimeCounter <= 0 || fGarbageLeft <= 0 || playerRef.IsDed) && !resultScreenCalled)
        {
            if (playerRef.IsDed)
            {
                fTimeCounter = 0;
            }

            //Calculate the final score
            var collector = _scoreSystem.CalculateFinalScore(possibleTimeBonus, fTimeCounter, timeLimit);
            
            timeBonus = collector.Item1;
            finalScore = collector.Item2;
            //Bring Up Result Screen
            resultScreenCalled = true;
            timerIsActive = false;
            SceneManager.LoadScene("ResultScreen", LoadSceneMode.Additive);
        }
    }

    public void GarbageToScore(float scoreGet, float distance)
    {
        fGarbageLeft --;
        score = _scoreSystem.AddScore(scoreGet, distance);
        scoreNumText.text = score.ToString(@"00000000000");
    }

    public (float, float, float) GiveResultValue()
    {
        return (score, timeBonus, finalScore);
    }

    public string GiveLevelName()
    {
        return sceneName;
    }
}
