using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dumpster : MonoBehaviour
{
    private Inventory _inventory;
    private GameManager _gameManager;
    
    // Start is called before the first frame update
    void Start()
    {
        _inventory = Inventory.instance;
        _gameManager = GameManager.instance;

    }

    private void OnTriggerEnter(Collider other)
    {
        var refObj = other.gameObject;
        var scoreCollecter = 0f;

        if (refObj.CompareTag("Player") )
        {
            if (_inventory.transform.childCount > 0)
            {
                scoreCollecter = _inventory.UnloadGarbage();
                _gameManager.GarbageToScore(scoreCollecter, 1);
            }
        }

        if (refObj.CompareTag("Garbage"))
        {
            refObj.SetActive(false);
            Garbage garbage = refObj.GetComponent<Garbage>();
            scoreCollecter = garbage.GiveScore();
            var thrownRef = garbage.GiveThrownPosition();

            var finalDistance = measureDistance(thrownRef);
            
            _gameManager.GarbageToScore(scoreCollecter, finalDistance);
        }
    }

    private float measureDistance(Vector3 thrownPosition)
    {
        var distanceMul = 0f;

        distanceMul = (transform.position - thrownPosition).magnitude;

        var roundedDistance = Mathf.Round(distanceMul);
        
        Debug.Log(roundedDistance);
        
        return roundedDistance;
    }
}