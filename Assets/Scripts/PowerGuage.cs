using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerGuage : MonoBehaviour
{
    #region Singleton

    public static PowerGuage instance;
    
    void Awake () 
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance of Inventory found!");
            return;
        }
        
        instance = this;
        
    }

    #endregion
    
    private Slider slider;
    [SerializeField] private Gradient _gradient;
    [SerializeField] private Image fill;
    
    private CanvasGroup powerGuageGroup;
    private bool fadeInCalled = false;
    private bool fadeOutCalled = false;
    [SerializeField] private float fadeInRate;
    [SerializeField] private float fadeOutRate;
    
    private void Start()
    {
        slider = GetComponent<Slider>();
        powerGuageGroup = GetComponent<CanvasGroup>();
        slider.value = slider.minValue;
        powerGuageGroup.alpha = 0;
    }

    public float SetValue(float value)
    {
        slider.value = value;
        fill.color = _gradient.Evaluate(slider.normalizedValue);
        return slider.value;
    }
    
    public void FadeIn()
    {
        fadeInCalled = true;
    }
    
    public void FadeOut()
    {
        fadeOutCalled = true;
    }

    private void Update()
    {
        
        if (fadeInCalled)
        {
            if (powerGuageGroup.alpha < 1)
            {
                powerGuageGroup.alpha += fadeInRate * Time.deltaTime;
                if (powerGuageGroup.alpha >= 1)
                {
                    fadeInCalled = false;
                }
            }
        }
        
        if (fadeOutCalled)
        {
            if (powerGuageGroup.alpha >= 0)
            {
                powerGuageGroup.alpha -= fadeOutRate * Time.deltaTime;
                slider.value -= fadeOutRate * Time.deltaTime;
                if (powerGuageGroup.alpha == 0 || fadeInCalled)
                {
                    fadeOutCalled = false;
                }
            }
        }
        
    }
}
