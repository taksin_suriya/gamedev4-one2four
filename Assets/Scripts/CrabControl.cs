using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using Cursor = UnityEngine.Cursor;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

public class CrabControl : MonoBehaviour
{
    #region Variables

    private Animator _animator;
    
    [SerializeField] private float MovementSpeed = 5f;
    private float SmoothTurnSpeed;
    private float SmoothTurnTime = 0.1f;

    [SerializeField] private float massLimit = 10f;
    private float mass;    

    [SerializeField] private float Gravity = -9.8f;
    private Vector3 Velocity;
    
    private Collider bodyCollider;
    private CharacterController CrabController;
    private Inventory _inventory;
    private Transform readiedGarbage;

    [SerializeField] private Transform MainCamera;
    public float Health = 100f;
    public bool IsDed = false;
    
    
    #region Jump Related Valiables
    
    [Header("[ Jump Settings ]")]
    
    [SerializeField] private float JumpHeight = 3f;
    
    [SerializeField] private float JumpBufferLenght = 0.2f;
    private float BufferCounter;
    
    [SerializeField] private float HangTimeLenght = 0.2f;
    private float HangCounter;
    
    #endregion
    
    
    #region Ground Checker
    
    [Header("[ Ground Checker Settings ]")]
    
    [SerializeField] private LayerMask GroundMask;
    [SerializeField] private bool IsGround;
    private bool CheckHit;
    
    private Vector3 GroundCheckerPosition;
    [SerializeField] private Vector3 GroundCheckerOffset;
    [SerializeField] private float GroundCheckerRadius = 1f;
    
    #endregion


    #region Grab Area Checker
    
    [Header("[ Grab Settings ]")]
    
    [SerializeField] private float GrabAreaRadius;
    
    [Range(1f, 90f)]
    [SerializeField] private float GrabAreaArcAngle;
    [SerializeField] private LayerMask garbageMask;
    
    private bool IsInGrabArea;
    private bool IsHolding;
    
    #endregion


    #region Throwing

    [Header("[ Throw Settings ]")] 
    
    [Range(0,2)]
    [SerializeField] private float guageFillRate;
    [SerializeField] private float maxThrowForce;

    [SerializeField] private PowerGuage powGuage;
    
    private float cyclingPowScale;
    private float powScale;
    private float throwForce;
    
    private bool _isHoldingThrow = false;

    private Vector3 thrownPosition;
    
    #endregion


    #endregion
    
    // Start is called before the first frame update
    void Start()
    {
        _inventory = Inventory.instance;
        bodyCollider = GetComponent<Collider>();
        CrabController = GetComponent<CharacterController>();
        Cursor.lockState = CursorLockMode.Locked;
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //Movement Multiplier
        float moventMul = 1 - (mass / massLimit);
        
        #region Movement
        
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;

        //Turning
        if (direction.magnitude >= 0.1)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg +
                                MainCamera.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle,
                                                ref SmoothTurnSpeed, SmoothTurnTime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);
            
            Vector3 MoveDirection = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
            CrabController.Move(MoveDirection.normalized * ((MovementSpeed * moventMul) * Time.deltaTime));
        }
        
        //Step Offset fix
        if(IsGround)
        {
            CrabController.stepOffset = 0.3f;
        }
        else
        {
            CrabController.stepOffset = 0f;
        }
        
        #endregion

        
        #region Ground Check 
        
        var Bounds = bodyCollider.bounds;
        GroundCheckerPosition = Bounds.center + GroundCheckerOffset;

        bool CheckHit = Physics.CheckSphere(GroundCheckerPosition, GroundCheckerRadius, GroundMask);
        
        if (CheckHit)
            IsGround = true;
        else
            IsGround = false;
        
        #endregion

        
        #region Gravity
        
        //Applying Gravity
        Velocity.y += Gravity * Time.deltaTime;
        CrabController.Move(Velocity * Time.deltaTime);
        
        //Gravity Reset
        if (IsGround && Velocity.y < 0)
        {
            Velocity.y = -2f;
        }
        
        #endregion
        

        #region Jumping
        
        BufferCounter -= Time.deltaTime;
        if (Input.GetButtonDown("Jump"))
        {
            BufferCounter = JumpBufferLenght; 
        }

        HangCounter -= Time.deltaTime;
        if (IsGround)
        {
            HangCounter = HangTimeLenght;
        }
        
        BufferCounter -= Time.deltaTime;
        if ((BufferCounter > 0) && (HangCounter > 0))
        {
            BufferCounter = 0;
            HangCounter = 0;
            Velocity.y = Mathf.Sqrt((JumpHeight * moventMul) * -2 * Gravity);
        }

        if (Input.GetButtonUp("Jump") && Velocity.y > 0)
        {
            Velocity.y = (Velocity.y * 0.5f);
        }
        
        #endregion


        #region Grabing

        Collider[] grabAreaHit = Physics.OverlapSphere(Bounds.center, GrabAreaRadius,garbageMask);

        if (_inventory.gameObject.transform.childCount > 0)
        {
            IsHolding = true;
        }
        else
        {
            IsHolding = false;
        }

        if (!IsHolding)
        {
            if (grabAreaHit.Length > 0)
            {
                Vector3 vectorToCollider = (grabAreaHit[0].transform.position - transform.position).normalized;
                var Dot = Vector3.Dot(vectorToCollider, transform.forward);
                //Debug.Log(Dot);

                // 180 degree arc, change 0 to 0.5 for a 90 degree "pie"
                if (Dot > 1 - (GrabAreaArcAngle / 90))
                {
                    IsInGrabArea = true;
                    if (Input.GetButtonDown("Grab"))
                    {
                        mass = _inventory.AddItem(grabAreaHit[0].gameObject);
                    }
                }
                else IsInGrabArea = false;
            }
        }


        #endregion


        #region Throwing / Dropping


        if (_inventory.transform.childCount > 0)
        {
            if (Input.GetMouseButtonDown(0))
            {
                powGuage.FadeIn();
                powGuage.SetValue(0);
                cyclingPowScale = 0;

                readiedGarbage = _inventory.gameObject.transform.GetChild(0);
                _isHoldingThrow = true;
            }
            else if (Input.GetMouseButton(0))
            {
                //Power Charge
                cyclingPowScale += guageFillRate * Time.deltaTime;
                float finalPowScale = Mathf.PingPong(cyclingPowScale, 1);

                powScale = powGuage.SetValue(finalPowScale);

                throwForce = maxThrowForce * powScale;
            }
            else if (Input.GetMouseButtonUp(0))
            {
                //Save thrown position
                thrownPosition = transform.position;
                
                //Shoot
                readiedGarbage.gameObject.GetComponent<Garbage>().Throw(MainCamera.forward * throwForce);
                powGuage.FadeOut();
                _isHoldingThrow = false;
            }


            if (Input.GetButtonDown("Drop") && !_isHoldingThrow)
            {
                readiedGarbage = _inventory.gameObject.transform.GetChild(0);
                readiedGarbage.gameObject.GetComponent<Garbage>().Throw((transform.up + (-transform.forward)) * 2f);
            }
        }
        else
            mass = 0;


        #endregion


        CheckHealth();

        AnimationUpdate(direction.magnitude);
    }

    
    
    private void OnDrawGizmos() //Note: Most Gizmos can't be Rotate
    {
        if (IsGround)
        {Gizmos.color = Color.green;}
        else
        {Gizmos.color = Color.red;}
        
        Gizmos.DrawWireSphere(GroundCheckerPosition, GroundCheckerRadius);
        
        
        if (IsInGrabArea)
        {Gizmos.color = Color.green;}
        else
        {Gizmos.color = Color.red;}

        var position = transform.position;
        var forward = transform.forward;
        
        Gizmos.DrawWireSphere(position, GrabAreaRadius);
        Gizmos.DrawLine(position, position + (forward * GrabAreaRadius)); //Transform Forward Vector
        
        var vectorRotatedY = Quaternion.Euler(new Vector3(0, GrabAreaArcAngle, 0)) * forward;
        var vectorRotatedYn = Quaternion.Euler(new Vector3(0, -GrabAreaArcAngle, 0)) * forward;

        Gizmos.DrawLine(position, position + (vectorRotatedY * GrabAreaRadius));
        Gizmos.DrawLine(position, position + (vectorRotatedYn * GrabAreaRadius));
    }

    private void CheckHealth()
    {
        Health -= Time.deltaTime * 2;

        if (Health <= 0)
        {
            Health = 0;
            IsDed = true;
            gameObject.SetActive(false);
        }
    }

    public void TakeDamage(float Damage)
    {
        Health -= Damage;
    }

    private void AnimationUpdate(float DirMag)
    {
        _animator.SetFloat("DirectionMagnitude", DirMag);
    }
}