using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ResultScreen : MonoBehaviour
{
    private GameManager _gameManager;

    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private TextMeshProUGUI timeBonusText;
    [SerializeField] private TextMeshProUGUI finalScoreText;
    
    // Start is called before the first frame update
    void Start()
    {
        _gameManager = GameManager.instance;
        Cursor.lockState = CursorLockMode.None;

        var collector = _gameManager.GiveResultValue();
        scoreText.text = Mathf.RoundToInt(collector.Item1).ToString();
        timeBonusText.text = Mathf.RoundToInt(collector.Item2).ToString();
        finalScoreText.text = Mathf.RoundToInt(collector.Item3).ToString();
    }

    public void Retry()
    {
        SceneManager.LoadScene(_gameManager.GiveLevelName());
    }

    public void BackToMain()
    {
        //SceneManager.LoadScene();
    }
}
