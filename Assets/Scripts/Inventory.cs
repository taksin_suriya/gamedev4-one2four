using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    
    #region Singleton

    public static Inventory instance;

    private Transform garbage;
    
    void Awake () 
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance of Inventory found!");
            return;
        }
        
        instance = this;
        
    }

    #endregion

    private PowerGuage _powerGuage;

    private void Start()
    {
        _powerGuage = PowerGuage.instance;
    }
    
    public float AddItem(GameObject Garbage)
    {
        Garbage.transform.parent = transform;
        Garbage.SetActive(false);
        Garbage.GetComponent<Collider>().enabled = false;
        TakeOut();

        var mass = Garbage.GetComponent<Garbage>().GiveMassInfo();
        return mass;
    }

    private void TakeOut()
    {
        if (transform.childCount > 0)
        {
            garbage = gameObject.transform.GetChild(transform.childCount - 1);
            garbage.gameObject.SetActive(true);
            garbage.position = transform.position + new Vector3(0, 1.5f, 0);
            garbage.rotation = Quaternion.identity;
        }
    }

    public float UnloadGarbage()
    {
        if (transform.childCount > 0)
        {
            _powerGuage.FadeOut();
            
            garbage = gameObject.transform.GetChild(transform.childCount - 1);
            garbage.parent = null;
            garbage.gameObject.SetActive(false);

            return garbage.GetComponent<Garbage>().GiveScore();
            //Delete or Reuse
        }

        return 0;
    }
    
    
}
