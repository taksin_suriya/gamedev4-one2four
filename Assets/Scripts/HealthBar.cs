using UnityEngine;
using UnityEngine.UI;

namespace TangBranch.Scripts
{
    public class HealthBar : MonoBehaviour
    {
        public Slider _HealthBar;

        public float CurrentHealth;

        private float MaxHealth = 100f;

        private CrabControl Player;
        // Start is called before the first frame update
        void Start()
        {
            _HealthBar = GetComponent<Slider>();
            Player = FindObjectOfType<CrabControl>();
        }

        // Update is called once per frame
        void Update()
        {
            CurrentHealth = Player.Health;
            _HealthBar.value = CurrentHealth/MaxHealth;
        }
    }
}
