using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Garbage : MonoBehaviour
{
    private int itemCount;
    private Rigidbody Rb;
    private Collider _collider;
    [SerializeField] private float score;
    private Vector3 thrownPosition;
    
    // Start is called before the first frame update

    void Awake()
    {
        Rb = GetComponent<Rigidbody>();
        _collider = GetComponent<Collider>();
    }

    private void OnEnable()
    {
        Rb.collisionDetectionMode = CollisionDetectionMode.Discrete;
        Rb.isKinematic = true;
        itemCount=GameObject.FindGameObjectsWithTag("Item").Length;
        Debug.Log(itemCount);
    }

    public void Throw(Vector3 Force)
    {
        _collider.enabled = true;
        transform.parent = null;
        Rb.isKinematic = false;
        Rb.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
        Rb.AddForce(Force, ForceMode.Impulse);

        thrownPosition = transform.position;
    }

    private void OnCollisionStay(Collision other)
    {
        if (other.gameObject.layer == 6)
        {
            thrownPosition = transform.position;
            Debug.Log("Thrown Position Reset");
        }
    }

    public float GiveMassInfo()
    {
        return Rb.mass;
    }

    public float GiveScore()
    {
        return score;
    }

    public Vector3 GiveThrownPosition()
    {
        return thrownPosition;
    }
}
