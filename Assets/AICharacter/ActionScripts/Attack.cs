using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : State
{
    public AIFSMScript localFSM { get; set; }
    
    public Attack(FSM fsm) : base(fsm)
    {
        localFSM = (AIFSMScript)fsm;

        //TODO: delete this line
        localFSM.CurrentStateName = this.GetType().Name;
    }
    
    public override void Enter()
    {
        localFSM.Anim.SetTrigger("AIAttack");

        localFSM.StopAINavigation();

        base.Enter();
    }
    
    public override void Update()
    {
        if (!localFSM.IsPlayerInAttackRange)
        {
            if (localFSM.IsPlayerBehind())
            {
                FSM.NextState = new Idle(FSM);
                this.StateStage = StateEvent.EXIT;
            }
            else
            {
                FSM.NextState = new Run(FSM);
                this.StateStage = StateEvent.EXIT;
            }
        }
    }
    
    public override void Exit()
    {
        localFSM.Anim.ResetTrigger("AIAttack");

        localFSM.StopAINavigation();


        base.Exit();
    }
    
}
