using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Idle : State
{
    public AIFSMScript localFSM { get; set; }
    private float delayTime;
    
    public Idle(FSM fsm) : base(fsm)
    {
        localFSM = (AIFSMScript)fsm;

        //TODO: delete this line
        localFSM.CurrentStateName = this.GetType().Name;
    }
    
    public override void Enter()
    {
        localFSM.Anim.SetTrigger("AIIdle");
        delayTime = Random.Range(2, 5);
        base.Enter();
    }
    
    public override void Update()
    {
            
        delayTime -= Time.deltaTime;
        if (delayTime <= 0)
        {
            //Prepare to go to the next state, Patrol
            localFSM.NextState = new Patrol(localFSM);
            this.StateStage = StateEvent.EXIT;
        }
    }
    
    public override void Exit()
    {
        localFSM.Anim.ResetTrigger("AIIdle");
        base.Enter();
    }
    
}
