using KS.Character;
using UnityEngine;

    [RequireComponent(typeof (UnityEngine.AI.NavMeshAgent))]
    [RequireComponent(typeof (ThirdPersonCharacterKSModified))]
    public class AICharacterControlKSModified : MonoBehaviour
    {
        public UnityEngine.AI.NavMeshAgent Agent { get; private set; }             // the navmesh agent required for the path finding
        public ThirdPersonCharacterKSModified Character { get; private set; } // the character we are controlling
        public Transform target;                                    // target to aim for


        private void Start()
        {
            // get the components on the object we need ( should not be null due to require component so no need to check )
            Agent = GetComponentInChildren<UnityEngine.AI.NavMeshAgent>();
            Character = GetComponent<ThirdPersonCharacterKSModified>();

	        Agent.updateRotation = false;
	        Agent.updatePosition = true;
        }


        private void Update()
        {
            if (target != null)
                Agent.SetDestination(target.position);

            if (Agent.remainingDistance > Agent.stoppingDistance)
            {
                Character.Move(Agent.desiredVelocity, false, false);
            }
            else
            {
                Character.Move(Vector3.zero, false, false);
                target = null;
            }
        }


        public void SetTarget(Transform target)
        {
            this.target = target;
        }
    }
