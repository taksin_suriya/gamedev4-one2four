using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : State
{
    public AIFSMScript localFSM { get; set; }
        
    private int currentWaypointIdx = -1;
    private static readonly int AIWalk = Animator.StringToHash("AIWalk");

    public Patrol(FSM fsm) : base(fsm)
    {
        localFSM = (AIFSMScript)fsm;

        //localFSM.Agent.speed;
            
        //TODO: delete this line
        localFSM.CurrentStateName = this.GetType().Name;
    }
    
    public override void Enter()
    {
        float lastDist = Mathf.Infinity; // Store distance between NPC and waypoints.
            
        // Calculate closest waypoint by looping around each one and calculating the distance between the NPC and each waypoint.
        for (int i = 0; i < localFSM.wayPoints.Count; i++)
        {
            GameObject thisWP = localFSM.wayPoints[i].gameObject;
            float distance = Vector3.Distance(localFSM.NpcGameObject.transform.position, thisWP.transform.position);
            if(distance < lastDist)
            {
                currentWaypointIdx = i;
                lastDist = distance;

                localFSM.AIChar.SetTarget(thisWP.transform);
            }            
        }
        localFSM.Anim.SetTrigger(AIWalk);
            
        //Proceed to the next stage of the FSM
        base.Enter();
    }

    public override void Update()
    {
        if (localFSM.AIChar.target != null)
            localFSM.Agent.SetDestination(localFSM.AIChar.target.position);

        if (localFSM.Agent.remainingDistance > localFSM.Agent.stoppingDistance)
        {
            //Move the agent
            localFSM.ThirdPersonChar.Move(localFSM.Agent.desiredVelocity, false, false);
        }
        else
        {
            //increase waypoint index
            if (currentWaypointIdx >= localFSM.wayPoints.Count - 1)
                currentWaypointIdx = 0;
            else
                currentWaypointIdx++;

            //Set target to the next waypoint
            localFSM.AIChar.SetTarget(localFSM.wayPoints[currentWaypointIdx]);
            //localFSM.Agent.SetDestination(localFSM.AIChar.target.position);

            //Stop the character movement
            localFSM.ThirdPersonChar.Move(Vector3.zero, false, false);
        }

        //
        if (localFSM.CanSeePlayer())
        {
            FSM.NextState = new Run(FSM);
            this.StateStage = StateEvent.EXIT;
        }
    }

    public override void Exit()
    {
            
        localFSM.Anim.ResetTrigger("AIWalk");
            
        base.Exit();
    }
}

