using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Run : State
{
    public AIFSMScript localFSM { get; set; }
    
    public Run(FSM fsm) : base(fsm)
    {
        localFSM = (AIFSMScript)fsm;
        localFSM.Agent.speed=12f;
        //TODO: delete this line
        localFSM.CurrentStateName = this.GetType().Name;
            
    }
    public override void Enter()
    {
        localFSM.AIChar.SetTarget(localFSM.player);
        localFSM.Agent.SetDestination(localFSM.player.position);
            
        localFSM.Anim.SetTrigger("AIRun");

        base.Enter();
    }
    
    public override void Update()
    {
        #region Chasing the player

        localFSM.Agent.SetDestination(localFSM.player.position);
            
        if (localFSM.Agent.remainingDistance > localFSM.Agent.stoppingDistance)
        {
            //Move the agent
            localFSM.ThirdPersonChar.Move(localFSM.Agent.desiredVelocity*8f, false, false);
        }

        #endregion
            
        #region Checking if the player is escaped successfully/behind

        if (localFSM.IsPlayerBehind())
        {
            if (Random.Range(0, 100) > 50)
            {
                FSM.NextState = new Idle(FSM);
                this.StateStage = StateEvent.EXIT;
            }
            else
            {
                FSM.NextState = new Patrol(FSM);
                this.StateStage = StateEvent.EXIT;
            }
        }

        #endregion
            
            
       if (localFSM.IsPlayerInAttackRange)
        {
            FSM.NextState = new Attack(FSM);
            this.StateStage = StateEvent.EXIT;
        }
    }
    
    public override void Exit()
    {
        localFSM.Anim.ResetTrigger("AIRun");

        localFSM.StopAINavigation();


        base.Exit();
    }
    
    
}
