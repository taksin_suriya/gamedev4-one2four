using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class _AttackTrigger : MonoBehaviour
{
    public UnityEvent callWhenTriggerEnter;
    public UnityEvent callWhenTriggerExit;
    
    [SerializeField] private Collider playerCollider;
    // Start is called before the first frame update
    void Start()
    {
        playerCollider = GameObject.FindWithTag("Player").GetComponent<Collider>();
    }
    
    private void OnTriggerEnter(Collider other)
    {

        if (other == playerCollider)
        {
            playerCollider.gameObject.GetComponent<CrabControl>().TakeDamage(10);
        
            callWhenTriggerEnter.Invoke();
        }
            
    }
    
    private void OnTriggerExit(Collider other)
    {
        if(other == playerCollider)
            callWhenTriggerExit.Invoke();
    }
    
    
}
